package id.vincenttp.subscriptionexample;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.vending.billing.IInAppBillingService;

import id.vincenttp.subscriptionexample.util.IabBroadcastReceiver;
import id.vincenttp.subscriptionexample.util.IabHelper;
import id.vincenttp.subscriptionexample.util.IabResult;
import id.vincenttp.subscriptionexample.util.Inventory;
import id.vincenttp.subscriptionexample.util.Purchase;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhNOEKol6F22/e+6kZFacAKfOteVzm47GCVkouIpTmw8r230gNiT2l4E0Gy3YtBmflm7hrsxIDIQ4RTknncmFXlSZ+R8YiDtpW8l881Nv1uGzvcKkIuVWNS5f3914fOEqXSHlrZaN/SMhC/fsDTSlwbq+8g8VB8NJ3yrK3lGMswTsZ+wafJ40kFV/a2tZuppJJATExwEuIV9EFyuRoHUtof9C5H4cwEf4eBbT8i+i6QHdMjukOlhr/Es+sLfT9D1N+9zNKdxRe5QYt/noDYPKcejO0WwoCw4c+BmLoWNrY3Rtj7l6VWBo6p+nxUDXE3HaW/PhrUYBl69SskiqYh1chwIDAQAB";
    static final String SKU_MONTHLY = "subs_monthly";
    static final String TAG = "vincenttpsubscribeexam";
    static final int REQUEST_CODE_SUBSCRIBE_MONTHKY = 22;

    IabHelper mHelper;
    TextView userType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnSubscribe = findViewById(R.id.btn_subscribe);
        userType = findViewById(R.id.user_type);
        btnSubscribe.setOnClickListener(this);

        mHelper = new IabHelper(this, base64EncodedPublicKey);
        mHelper.enableDebugLogging(true);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.d(TAG, "In-app Billing setup failed: " + result);
                } else {
                    Log.d(TAG, "In-app Billing is set up OK");
                    try {
                        mHelper.queryInventoryAsync(mReceivedInventoryListener);
                    } catch (IabHelper.IabAsyncInProgressException e) {
                        Log.d(TAG, "Error querying inventory. Another async operation in progress.");
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        try {
            mHelper.launchSubscriptionPurchaseFlow(this, SKU_MONTHLY, REQUEST_CODE_SUBSCRIBE_MONTHKY, mPurchaseFinishedListener);
        } catch (IabHelper.IabAsyncInProgressException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mHelper == null) return;

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }
        else {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
        }
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if (result.isFailure()) {
                Log.d(TAG, result.getMessage());
                return;
            }
            else {
                Log.d(TAG, result.getMessage());
                try {
                    mHelper.queryInventoryAsync(mReceivedInventoryListener);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    e.printStackTrace();
                }
            }
            Log.d(TAG, purchase.getSku());
        }
    };

    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            if (result.isFailure()) {
                Log.d(TAG, result.getMessage());
            } else {
                Purchase subsMonthly = inventory.getPurchase(SKU_MONTHLY);
                if (subsMonthly.isAutoRenewing()){
                    userType.setText("PREMIUM");
                }else {
                    userType.setText("BASIC");
                }
            }
        }
    };
}
